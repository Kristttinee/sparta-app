import type { INPUT_TYPES } from "../utils/constants/Input";

export type InputTypes = typeof INPUT_TYPES.TEXT | typeof INPUT_TYPES.NUMERIC