export type IconsPropTypes = {
    width?: string;
    height?: string;
    color?: string;
    dataTestId?: string;
}