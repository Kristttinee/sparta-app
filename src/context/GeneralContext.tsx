import React, { createContext, useContext, useEffect, useState } from "react";

interface GeneralContextInterface {
	logIn: (token: string) => void;
	logOut: () => void;
	authToken: string | null;
}

interface ContextProps {
	children: React.ReactNode;
}

const GeneralContext = createContext({} as GeneralContextInterface);

export const useGeneralContext = () => {
  const context = useContext(GeneralContext);
  if (!context) {
    throw new Error("useGeneralContext must be used within a provider");
  }
  return context;
};

export const GeneralProvider = ({ children }: ContextProps) => {
  const [authToken, setAuthToken] = useState<string | null>(null);

  const logIn = (token: string) => {
    setAuthToken(token);
    localStorage.setItem("authToken", token);
  };

  const logOut = () => {
    setAuthToken("");
    localStorage.removeItem("authToken");
  };

  useEffect(() => {
    // Check for an authentication token in localStorage after each render
    const storedToken = localStorage.getItem("authToken");
    if (storedToken !== authToken) {
      setAuthToken(storedToken);
    }
  }, [authToken]);

  const values = {
    authToken, 
    logIn, 
    logOut,
  }

  return (
    <GeneralContext.Provider value={values}>
      {children}
    </GeneralContext.Provider>
  );
};
