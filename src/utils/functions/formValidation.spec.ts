import { isValidEmail, isValidPassword } from "./formValidation";

describe("Form validation functions", () => {
  it("should check if is valid password", () => {
    const minNumberCharacters = 4;
    expect(isValidPassword("123456")).toBeTruthy();
    expect(isValidPassword("12345")).toBeFalsy();
    expect(isValidPassword("123", minNumberCharacters)).toBeFalsy();
    expect(isValidPassword("1234", minNumberCharacters)).toBeTruthy();
  });

  it("should check if is valid email", () => {
    expect(isValidEmail("email")).toBeFalsy();
    expect(isValidEmail("email@")).toBeFalsy();
    expect(isValidEmail("email@email")).toBeFalsy();
    expect(isValidEmail("email@email.")).toBeFalsy();
    expect(isValidEmail("emailemail.com")).toBeFalsy();
    expect(isValidEmail("mysite.ourearth.com")).toBeFalsy();
    expect(isValidEmail("email@.com.my")).toBeFalsy();
    expect(isValidEmail("@you.me.net")).toBeFalsy();
    expect(isValidEmail("mysite123@gmail.b")).toBeFalsy();
    expect(isValidEmail("mysite@.org.org")).toBeFalsy();
    expect(isValidEmail(".mysite@mysite.org")).toBeFalsy();
    expect(isValidEmail("mysite()*@gmail.com")).toBeFalsy();
    expect(isValidEmail("mysite..1234@yahoo.com")).toBeFalsy();
    expect(isValidEmail(" email@email.com")).toBeFalsy();
    expect(isValidEmail("email@email.com ")).toBeFalsy();
    expect(isValidEmail(" email@email.com ")).toBeFalsy();
    expect(isValidEmail("email@email.com")).toBeTruthy();
  });
});
