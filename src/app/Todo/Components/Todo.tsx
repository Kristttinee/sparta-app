import { useGeneralContext } from "../../../context/GeneralContext";
import { INPUT_TYPES } from "../../../utils/constants/Input";
import MainButton from "../../Common/Button"
import ConfirmationModal from "../../Common/ConfirmationModal";
import Input from "../../Common/Input";
import Modal from "../../Common/Modal";
import TodoController from "../Controller/TodoController";

import "../../../css/Todo.scss";
import "../../../css/Button.scss";

import TodoList from "./TodoItem";

const Todo: React.FC = () => {
  const { logOut } = useGeneralContext();

  const {
    handleTaskInput,
    handleOnKeyDown,
    handleRemoveTask,
    handleSaveTask,
    handleCheckTask,
    confirmDelete,
    handleEditTask,
    isEditModalOpen,
    isConfirmationModalOpen,
    setIsConfirmationModalOpen,
    onCloseEditModal,
    listOfTasks,
    checkedTaskId,
    newTask,
    editedTaskId,
  } = TodoController()

  const InputContainer = () => (
    <div className="todo-input-container">
        <Input 
            dataTestId="taskInputId"
            inputType={INPUT_TYPES.TEXT}
            inputValue={newTask} 
            onChangeValue={(e) => handleTaskInput(e) } 
            onKeyDown={(e) => handleOnKeyDown(e)}
            containerClassName="todo-input-container"
        />
        <MainButton dataTestid="addNewTaskButtonId" label={editedTaskId ? "Edit" : "Insert"} onClick={handleSaveTask} className="add-task-button" />
    </div>
  )
    
    return (

      <div data-testid="todo-container" className="todo-container">
          <InputContainer />
          <div>
              <TodoList 
                dataTestId="taskItem"
                listOfTasks={listOfTasks}
                handleRemoveTask={handleRemoveTask} 
                handleEditTask={handleEditTask}
                handleCheckTask={handleCheckTask} 
                checkedTaskId={checkedTaskId}
              />
          </div>
          <MainButton label="Sign out" onClick={() => logOut()} />
          {isEditModalOpen && 
            <Modal isOpen={isEditModalOpen} onClose={() => onCloseEditModal()}>
                <p>Type to edit the task</p>
                <InputContainer />
            </Modal>
          }
          {isConfirmationModalOpen && 
            <ConfirmationModal 
              isOpen={isConfirmationModalOpen} 
              onClose={() => setIsConfirmationModalOpen(false)}
              onConfirm={confirmDelete} 
              message="¿Are you sure you want to delete the task?"
            />
          }
      </div>
    )
}

export default Todo