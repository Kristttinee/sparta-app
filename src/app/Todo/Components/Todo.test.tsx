import { fireEvent, render, screen } from "@testing-library/react";

import Todo from "./Todo";

describe("Todo Component", () => {
  it("should render correctly", () => {
    const component = render(<Todo />)
    expect(component).toMatchSnapshot()
  })

  it("should add new task correctly", () => {
    render(<Todo />)
  
    const addNewTaskButton = screen.getByTestId("addNewTaskButtonId")
    const taskInput = screen.getByTestId("taskInputId")

    fireEvent.change(taskInput, { target: { value: "task2" } })
    fireEvent.click(addNewTaskButton)

    const listOfTasks = screen.getAllByTestId("taskItem")

    expect(listOfTasks.length).toBe(1)
  })
})