import { fireEvent, render, screen } from "@testing-library/react";

import Todo from "./Todo";
import TodoList from "./TodoItem";

describe("Todo list Component", () => {
    beforeEach(() => {
        jest.clearAllMocks();
      });

    const handleRemoveTask = jest.fn();
    const handleEditTask = jest.fn()
    const handleCheckTask = jest.fn()
    
    const listOfTasks=[{ id: "123", task: "task1", isChecked: false },{ id: "124", task: "task2", isChecked: false }]

    it("should render correctly", () => {
        const component = render(<TodoList listOfTasks={listOfTasks} handleRemoveTask={handleRemoveTask} handleEditTask={handleEditTask} handleCheckTask={handleCheckTask} />);
        expect(component).toMatchSnapshot()
    })

    it("should handle remove task", () => {
        render(<Todo />)
        render(<TodoList listOfTasks={listOfTasks} handleRemoveTask={handleRemoveTask} handleEditTask={handleEditTask} handleCheckTask={handleCheckTask} />);
        
        const deleteButton = screen.getByTestId("deleteItemButtonId2")
        const listOfTasksItem = screen.getAllByTestId("taskItem")

        fireEvent.click(deleteButton)

        expect(handleRemoveTask).toHaveBeenCalledWith("124")
        expect(listOfTasksItem.length).toBe(1)
    })
})