import CheckIcon from "../../../assets/CheckIcon";
import DeleteIcon from "../../../assets/DeleteIcon";
import EditIcon from "../../../assets/EditIcon";
import MainButton from "../../Common/Button";

import "../../../css/TodoItem.scss";
import "../../../css/Button.scss";

type TodoListProps = {
    listOfTasks: { id: string; task: string, isChecked?: boolean }[];
    handleRemoveTask: (id: string) => void;
    handleEditTask: (id: string) => void;
    handleCheckTask: (id: string) => void;
    checkedTaskId?: string;
    dataTestId?: string;
}

const TodoList = ({ listOfTasks, handleRemoveTask, handleEditTask, handleCheckTask, dataTestId }: TodoListProps) => (
        <ul data-testid={dataTestId} className="todo-list-container">
            {listOfTasks.map(({ id, task, isChecked }, index) => (
                <div key={index} className="tasks-container">
                    <div className="task-wrapper">
                        <span>
                        {++index}
                        </span>
                        <li>
                            {task}
                        </li>
                    </div>
                    <div>
                        <MainButton dataTestid={`deleteItemButtonId${index}`} icon={<DeleteIcon />} onClick={() => handleRemoveTask(id)} className="button"/> 
                       {!isChecked && <MainButton icon={<EditIcon />} onClick={() => handleEditTask(id)}  className="button" isDisabled={isChecked}/>} 
                        <MainButton dataTestid={`checkItemButtonId${index}`}  icon={<CheckIcon dataTestId={`checkIconId${index}`} color={isChecked ? "green" : "white"} />} onClick={() => handleCheckTask(id)}  className="button" /> 
                    </div>

                </div>
            ))}
        </ul>
)

export default TodoList;