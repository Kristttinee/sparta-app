import { useState } from "react";

const TodoController = () => {
    const [newTask, setnewTask] = useState<string>("")
    const [listOfTasks, setListOfTasks] = useState<{ 
      id: string; 
      task: string, 
      isChecked?: boolean;
    }[]>([])
    const [editedTaskId, setEditedTaskId] = useState<string>("")
    const [isEditModalOpen, setIsEditModalOpen] = useState<boolean>(false)
    const [isConfirmationModalOpen, setIsConfirmationModalOpen] = useState<boolean>(false)
    const [deleteTaskId, setDeleteTaskId] = useState<string>("");
    const [checkedTaskId, setCheckedTaskId] = useState<string>("");
  
  
    const handleTaskInput = (e: React.ChangeEvent<HTMLInputElement>) => {
      e.preventDefault()
      setnewTask(e.target.value)
    }
  
    const generateUniqueId = () => {
      // replace with uuid library 
      return Math.random().toString(36).substring(2) + Date.now().toString(36);
    };

    const onCloseEditModal = () => {
      setIsEditModalOpen(false)
      setnewTask("")
      setEditedTaskId("")
    }
  
    const handleSaveTask = () => {
      if(!newTask) return;
      
      if (editedTaskId) {
          const editedTasks = listOfTasks.map((task) => task.id === editedTaskId ? { ...task, task: newTask, isChecked: false } : task)
          setListOfTasks(editedTasks)
          setnewTask("")
          setEditedTaskId("")
          setIsEditModalOpen(false)
      } else {
          const newTaskObject = { id: generateUniqueId(), task: newTask, isChecked: false };
          setListOfTasks([...listOfTasks, newTaskObject])
          setnewTask("")
      }
    }

    const handleCheckTask = (selectedTaskId: string) => {
      setCheckedTaskId(selectedTaskId)
      const checkedTasks = listOfTasks.map((task) => task.id === selectedTaskId ? { ...task, isChecked: true } : task)
      setListOfTasks(checkedTasks)

    }
  
    const handleOnKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => e.key === "Enter" &&  handleSaveTask()
  
    const handleRemoveTask = (selectedTaskId: string) => {
      setDeleteTaskId(selectedTaskId);
      setIsConfirmationModalOpen(true);
    }
  
    const confirmDelete = () => {
      if (deleteTaskId) {
        const filteredList = listOfTasks.filter(selectedTask => selectedTask.id !== deleteTaskId);
        setListOfTasks(filteredList);
        setDeleteTaskId("");
      }
      setIsConfirmationModalOpen(false);
    };
  
    const handleEditTask = (selectedTaskId: string) => {
      setEditedTaskId(selectedTaskId)
      setIsEditModalOpen(true)
  
      const selectedTaskList = listOfTasks.filter(taskObj => taskObj.id === selectedTaskId)
      const selectedTask = selectedTaskList[0].task;
      
      setnewTask(selectedTask)
    }

    return {
        handleTaskInput,
        handleOnKeyDown,
        handleRemoveTask,
        handleSaveTask,
        handleCheckTask,
        confirmDelete,
        handleEditTask,
        isEditModalOpen,
        isConfirmationModalOpen,
        setIsConfirmationModalOpen,
        setIsEditModalOpen,
        checkedTaskId,
        listOfTasks,
        newTask,
        onCloseEditModal,
        editedTaskId,
    }
}

export default TodoController