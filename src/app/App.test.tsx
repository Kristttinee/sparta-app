import { render } from "@testing-library/react";

import App from "./App";

describe("App Component", () => {
    it("should render correctly", () => {
      const component = render(<App />);
      expect(component).toMatchSnapshot()
    })
})