import MainButton from "./Button";
import Modal from "./Modal";

import "../../css/ConfirmationModal.scss";


interface ConfirmationModalProps {
    isOpen: boolean;
    onClose: () => void;
    onConfirm: () => void;
    message: string;
  }
  
  const ConfirmationModal = ({ isOpen, onClose, onConfirm, message }: ConfirmationModalProps) => {
    return (
      <Modal isOpen={isOpen} onClose={onClose}>
        <p>{message}</p>
        <div className="buttons-container">
          <MainButton className="modal-buttons" label="Yes" onClick={onConfirm} />
          <MainButton className="modal-buttons" label="No" onClick={onClose} />
        </div>
      </Modal>
    );
  };
  
  export default ConfirmationModal;
  