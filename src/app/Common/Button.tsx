type MainButtonProps = {
    className?: string;
    label?: string;
	onClick: (e: React.MouseEvent<HTMLButtonElement>) => void;
	icon?: React.ReactNode;
	isDisabled?: boolean;
	dataTestid?: string;
}

const MainButton = ({ label, onClick, className, icon, isDisabled, dataTestid }: MainButtonProps)  => {
    return (
			<button 
				className={`button-container ${className}`}
				onClick={onClick}
				disabled={isDisabled}
				data-testid={dataTestid}
			>
				{label}
				{icon}
			</button>
    )
}

export default MainButton;