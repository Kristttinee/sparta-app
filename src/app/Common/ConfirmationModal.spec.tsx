import { render } from "@testing-library/react";

import ConfirmationModal from "./ConfirmationModal";

describe("Confirmation modal component", () => {
    const isOpen = true
    const message = "message"
    it("should render correctly", () => {
        const component = render(
            <ConfirmationModal 
                isOpen={isOpen} 
                onClose={() => ({})} 
                onConfirm={() => ({})} 
                message={message}
            />
        );
        expect(component).toMatchSnapshot()
    })
})