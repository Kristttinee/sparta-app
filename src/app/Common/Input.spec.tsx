import { render } from "@testing-library/react";

import Input from "./Input";

describe("Input component", () => {
    it("should render correctly", () => {
        const component = render(
            <Input 
            inputType="text" 
            inputValue="value"
            onChangeValue={() => ({})}
            onKeyDown={() => ({})}
            />
        );
        expect(component).toMatchSnapshot()
    })
})