import { render } from "@testing-library/react";

import MainButton from "./Button";


describe("Button Component", () => {
    it("should render correctly", () => {
        const component = render(<MainButton onClick={() => ({})}  />);
        expect(component).toMatchSnapshot()
    })
})