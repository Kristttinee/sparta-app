import "../../css/Input.scss";
import type { InputTypes } from "../../types/Input";

type InputProps = {
    inputValue: string;
    onChangeValue: (e: React.ChangeEvent<HTMLInputElement>) => void ;
    onKeyDown?: React.KeyboardEventHandler<HTMLInputElement>
    placeholder?: string;
    inputType: InputTypes;
    containerClassName?: string;
    inputClassName?: string;
    dataTestId?: string;
}

const Input = ({ containerClassName, inputType, inputValue, onChangeValue, onKeyDown, placeholder, inputClassName, dataTestId }: InputProps) => {

    return (
        <div className={containerClassName}>
            <input 
                type={inputType} 
                value={inputValue} 
                onChange={(e) => onChangeValue(e)} 
                onKeyDown={onKeyDown}
                placeholder={placeholder} 
                autoFocus
                className={`input-field ${inputClassName}`}
                data-testid={dataTestId}
            />
        </div>
    )

}

export default Input;