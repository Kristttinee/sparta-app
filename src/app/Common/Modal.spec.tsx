import { fireEvent, render, screen } from "@testing-library/react";

import Modal from "./Modal";

describe("Modal component", () => {
    const isOpen = true
    const message = "message"
    
    it("should render correctly", () => {
        const component = render(
            <Modal 
               isOpen={isOpen}
               onClose={() => ({})}
            >
            {message}
            </Modal>
        );
        expect(component).toMatchSnapshot()
    })

    it("should handle onClose correctly", () => {
        const onClose = jest.fn()

        render(
            <Modal 
               isOpen={isOpen}
               onClose={onClose}
            >
            {message}
            </Modal>
        );

        const button = screen.getByTestId("modalCloseButton")
        fireEvent.click(button)

        expect(onClose).toHaveBeenCalled()
    })
})