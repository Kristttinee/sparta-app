import "../../css/Modal.scss";

type ModalProps = {
    children: React.ReactNode;
    isOpen: boolean;
    onClose: () => void;
}
const Modal = ({ children, isOpen, onClose }: ModalProps) => {
    if (!isOpen) return null
    return (
        <div className="modal-overlay">
            <div className="modal-content">
                <button data-testid="modalCloseButton" onClick={onClose}>
                    Close
                </button>
                {children}
            </div>
        </div>
    )
}

export default Modal;