import { useState } from "react";

import { useGeneralContext } from "../../../context/GeneralContext";
import { isValidEmail, isValidPassword } from "../../../utils/functions/formValidation";

const LoginController = () => {
    const [userEmail, setUserEmail] = useState<string>("")
    const [userPassword, setUserPassword] = useState<string>("")
    const [formError, setFormError] = useState<boolean>(false);
  
    const { logIn } = useGeneralContext();
  
    const handleLogin = (e: React.MouseEvent<HTMLButtonElement>) => {
      e.preventDefault()
  
      if(!isValidEmail(userEmail) || !isValidPassword(userPassword)) {
        setFormError(true)
        setUserEmail("")
        setUserPassword("")
  
        // eslint-disable-next-line no-console
        console.error("Authentication failed");
      } else {
        logIn("fakeAuthToken");
      }
    }

    return {
        userEmail,
        setUserEmail,
        userPassword,
        setUserPassword,
        formError,
        handleLogin
    }
}

export  default LoginController;