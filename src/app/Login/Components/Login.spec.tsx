import { fireEvent, render, screen } from "@testing-library/react";

import * as GeneralContextModule from "../../../context/GeneralContext";

import Login from "./Login";


describe("Login Component", () => {
    it("should render correctly", () => {
        const component = render(<Login />);

        const emailInput = screen.getByPlaceholderText("Email");
        const passwordInput = screen.getByPlaceholderText("Password");

        expect(emailInput).toBeInTheDocument();
        expect(passwordInput).toBeInTheDocument();
        expect(component).toMatchSnapshot()
    })

    it("should handle user input changes", () => {
        render(<Login />);
        const emailInput = screen.getByPlaceholderText("Email") as HTMLInputElement;
        const passwordInput = screen.getByPlaceholderText("Password") as HTMLInputElement;
    
        fireEvent.change(emailInput, { target: { value: "test@example.com" } });
        fireEvent.change(passwordInput, { target: { value: "password123" } });
    
        expect(emailInput.value).toBe("test@example.com");
        expect(passwordInput.value).toBe("password123");
      });
    
      it("should handle login with valid credentials", () => {
        const logInMock = jest.fn();
        const authToken = "fakeAuthToken";
        jest.spyOn(GeneralContextModule, "useGeneralContext").mockReturnValue({ logIn: logInMock,  logOut: jest.fn(), authToken  });
    
        render(<Login />);
        const emailInput = screen.getByPlaceholderText("Email");
        const passwordInput = screen.getByPlaceholderText("Password");
        const loginButton = screen.getByTestId("loginButtonId");
    
        fireEvent.change(emailInput, { target: { value: "test@example.com" } });
        fireEvent.change(passwordInput, { target: { value: "password123" } });
        fireEvent.click(loginButton);
    
        expect(logInMock).toHaveBeenCalledWith("fakeAuthToken");
      });
    
      it("should not login with invalid credentials", () => {
        const logInMock = jest.fn();
        const authToken = "fakeAuthToken";
        jest.spyOn(GeneralContextModule, "useGeneralContext").mockReturnValue({ logIn: logInMock,  logOut: jest.fn(), authToken });
    
        render(<Login />);
        const emailInput = screen.getByPlaceholderText("Email");
        const passwordInput = screen.getByPlaceholderText("Password");
        const loginButton = screen.getByTestId("loginButtonId");
    
        fireEvent.change(emailInput, { target: { value: "invalid-email" } });
        fireEvent.change(passwordInput, { target: { value: "short" } });
        fireEvent.click(loginButton);
    
        expect(logInMock).not.toHaveBeenCalled();
      });
})