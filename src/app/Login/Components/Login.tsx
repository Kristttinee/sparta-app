import "../../../css/Login.scss";
import "../../../css/Input.scss";
import "../../../css/Button.scss";

import MainLogo from "../../../assets/MainLogo";
import { INPUT_TYPES } from "../../../utils/constants/Input";
import MainButton from "../../Common/Button";
import Input from "../../Common/Input";
import LoginController from "../Controller/LoginController";

const Login: React.FC = () => {
  const {
    userEmail,
    setUserEmail,
    userPassword,
    setUserPassword,
    formError,
    handleLogin,
  } = LoginController()

  return (
    <div className="login-container">
      <div className="logo-container">
        <MainLogo/>
      </div>
      <form>
        <Input 
          inputType={INPUT_TYPES.TEXT} 
          inputValue={userEmail} 
          onChangeValue={(e) => setUserEmail(e.target.value)} 
          placeholder="Email" 
          inputClassName="text-input"
          containerClassName="login-input-container"
        />
        <Input 
          inputType={INPUT_TYPES.NUMERIC} 
          inputValue={userPassword} 
          onChangeValue={(e) => setUserPassword(e.target.value)} 
          placeholder="Password" 
          inputClassName={!formError ? "password-input" : ""}
          containerClassName="login-input-container"
        />
        {!!formError && <p className="error-message">Your email or password are incorrect. Please try again</p>}
      </form>
      <MainButton dataTestid="loginButtonId" label="Login" onClick={(e ) => handleLogin(e)} className="login-button" /> 
    </div>
  );
}

export default Login;