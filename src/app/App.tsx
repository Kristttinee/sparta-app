import "./App.css";

import { useGeneralContext } from "../context/GeneralContext";

import Login from "./Login/Components/Login";
import Todo from "./Todo/Components/Todo";


const App: React.FC = () => {
  const { authToken } = useGeneralContext();
  
  return (
      <div className="container">

      {authToken ? <Todo /> : ( 
        <div>
          <Login />
        </div>
      )}
      
      </div>
  );
}

export default App;
